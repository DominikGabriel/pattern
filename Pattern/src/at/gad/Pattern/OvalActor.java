package at.gad.Pattern;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor extends AbstractActor implements Observer {
	
	private Color color;
	private MoveStrategy moveStrategy;
	public OvalActor(MoveStrategy mr){
		super();
		this.moveStrategy = mr;
	}



	@Override
	public void draw(Graphics g) {
		g.setColor(this.color);
		g.drawOval(this.moveStrategy.getX(), this.moveStrategy.getY(), 30, 30);
		g.setColor(Color.white);
		
	}



	@Override
	public void inform() {
		this.color = Color.blue;
		
	}



	@Override
	public void move(GameContainer gc, int delta) {
		moveStrategy.move(delta);
		
	}

}
