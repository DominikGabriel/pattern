package at.gad.Pattern;

public interface Observer {

	public void inform();
}
