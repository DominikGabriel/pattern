package at.gad.Pattern;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class Player implements Actor, Observer {
	
	private float x,y,speed;
	private Color color;
	private List <Observer> observers;
	
	public Player() {
		super();
		this.x = 300;
		this.y = 300;
		this.speed = 1f;
		this.observers = new ArrayList<>();
	}

	public void addObserver(Observer observer) {
		this.observers.add(observer);
	}
	
	
	@Override
	public void draw(Graphics g) {
		g.fillRect(this.x, this.y, 50, 50);
		
	}

	@Override
	public void move(GameContainer container, int delta) {
		if (container.getInput().isKeyDown(Input.KEY_LEFT)) {
			this.x -= (float)delta * speed;
			System.out.println("x: " + this.x);
		}
		if (container.getInput().isKeyDown(Input.KEY_RIGHT)) {
			this.x += delta * speed;
			System.out.println("x: " + this.x);
		}
		
		if(this.x >700) {
			for (Observer observer : observers) {
				observer.inform();
			}
		}
		
	}

	@Override
	public void inform() {
		
		
	}

}
