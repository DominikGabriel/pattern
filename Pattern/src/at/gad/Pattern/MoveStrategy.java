package at.gad.Pattern;

public interface MoveStrategy {
	
	public void move(int delta);
	public float getX();
	public float getY();

}
