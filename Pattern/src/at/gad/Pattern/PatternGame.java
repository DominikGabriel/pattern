package at.gad.Pattern;

import java.util.ArrayList;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class PatternGame extends BasicGame {
	
	private MoveStrategy mr1;
	private MoveStrategy mr2;
	private MoveStrategy snowManDown;
	private OvalActor c1;
	private RectangleActor rect1;
	private Snowman snowy;
	private Player player;
	private ArrayList<Actor> actors;
	private ArrayList<Observer> observers;

	public PatternGame (){
		super("Pattern Game");
	}
	
	
	public void init(GameContainer container) throws SlickException {
		this.mr1 = new MoveRight(0,0,0.1f);
		this.mr2 = new MoveLeft(0,0,0.1f);
		this.rect1 = new RectangleActor(mr2);
		//this.snowManDown = new MoveDown(0,300,0.5f);
		this.c1 = new OvalActor(mr1);
		this.player = new Player();
		//this.snowy = new Snowman(snowManDown);
		this.player = new Player();
		this.actors = new ArrayList<>();
		this.observers = new ArrayList<>();
		this.actors.add(c1);
		this.actors.add(rect1);
		this.actors.add(player);
		//this.actors.add(snowy);
		
		this.player.addObserver(c1);
		this.player.addObserver(rect1);

	}
	
	public void keyPressed(int key, char c) {
		if (key == Input.KEY_SPACE) {
			for(Observer observer: observers) {
				observer.inform();
			}
		}
	}

	
	
	public void render(GameContainer container, Graphics g) {
		for (Actor actor: actors) {
			actor.draw(g);
		}
	}
	
	public void update(GameContainer container, int delta) {
		for (Actor actor: actors) {
			actor.move(container,delta);
		}
		
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new PatternGame());
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	
	
}
