package at.gad.Pattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Actor {
	
	public void draw (Graphics g);
	void move(GameContainer gc, int delta);

}
