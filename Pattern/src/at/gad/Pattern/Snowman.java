package at.gad.Pattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowman implements Actor {
	
	private MoveStrategy moveStrategy;
	public Snowman(MoveStrategy sm){
		super();
		this.moveStrategy = sm;
	}

	@Override
	public void draw(Graphics g) {
		g.drawOval(this.moveStrategy.getX(),this.moveStrategy.getY(), 30, 30);
		
	}

	@Override
	public void move(GameContainer gc, int delta) {
		moveStrategy.move(delta);
		
	}
}
	
