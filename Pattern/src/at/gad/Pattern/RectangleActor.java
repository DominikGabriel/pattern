package at.gad.Pattern;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectangleActor extends AbstractActor implements Observer{

	private Color color;
	private MoveStrategy moveStrategy;
	
	public RectangleActor (MoveStrategy mr){
		super();
		this.moveStrategy = mr;
		this.color = Color.white;
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		g.drawRect(this.moveStrategy.getX(), this.moveStrategy.getY(), 20, 20);
		g.setColor(Color.white);
	}


	@Override
	public void move(GameContainer container, int delta) {
		/*if (this.moveStrategy.getX() < 100) {
			this.color = Color.red;
		}*/
		moveStrategy.move(delta);
	}

	@Override
	public void inform() {
		this.color = Color.pink;
		
	}
}
