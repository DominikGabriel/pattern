package at.gad.Pattern;


public class MoveDown implements MoveStrategy {
	
	private float x,y;
	private double speed;
	
	public MoveDown(float x, float y, double speed)  {
	
		this.x = x;
		this.y = y;
		this.speed = speed; 
		
	}

	@Override
	public void move(int delta) {
		this.y++;
		
	}
	


	@Override
	public float getX() {
		return x;
		
	}

	@Override
	public float getY() {
		return y;
		
	}
	
	

}
