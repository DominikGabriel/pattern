package at.gad.Pattern;

import java.util.Random;

public class MoveRight implements MoveStrategy {
	
	private float x,y,speed;
	Random r = new Random();
	
	public MoveRight(float x, float y, float speed)  {
	
		this.x = r.nextInt(400);
		this.y = r.nextInt(400);
		this.speed = speed;
		
	}

	@Override
	public void move(int delta) {
		this.x += (float)delta*speed;
		if(this.x > 700) {
			this.x = 0;
		}
		
	}
	


	@Override
	public float getX() {
		return x;
		
	}

	@Override
	public float getY() {
		return y;
		
	}

	
	
	
	

}
