package at.gad.Pattern;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class AbstractActor implements Actor {

	protected MoveStrategy moveStrategy;

	@Override
	public void move(GameContainer container, int delta) {
		moveStrategy.move(delta);
		
	}

	@Override
	public void draw(Graphics g) {
		// TODO Auto-generated method stub
		
	}

}
