package at.gad.Pattern;


public class MoveUp implements MoveStrategy {
	
	private float x,y;
	private double speed;
	
	public MoveUp(float x, float y, double speed)  {
	
		this.x = x;
		this.y = y;
		this.speed = speed; 
		
	}

	@Override
	public void move(int delta) {
		this.y--;
		
	}
	


	@Override
	public float getX() {
		return x;
		
	}

	@Override
	public float getY() {
		return y;
		
	}
	
	

}
