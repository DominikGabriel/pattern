package at.gad.Pattern;

import java.util.Random;

public class MoveLeft implements MoveStrategy {
	
	private float x,y,speed;
	Random r = new Random();
	
	public MoveLeft(float x, float y, float speed)  {
	
		this.x = r.nextInt(200)+300;
		this.y = r.nextInt(200)+300;
		this.speed = speed;
		
	}

	@Override
	public void move(int delta) {
		this.x -= (float)delta * speed;
		if(this.x <10) {
			this.x = 700;
		}
	}
	


	@Override
	public float getX() {
		return x;
		
	}

	@Override
	public float getY() {
		return y;
		
	}
	
	
	
	
	

}
