package at.gad.Pattern;

public interface Observable {
	public void informAll();
	public void addObserver();

}
